/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TILE_HPP
#define TILE_HPP
#include <iostream>
#include <Shape.hpp>

/**
 * Entity representing a tile in the game.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Tile : public Shape
{
 public:
  /**
   * @param r row where the tile will be.
   * @param c column where the tile will be.
   */
  explicit Tile(Core* core, size_t r, size_t c);
  virtual ~Tile();
  
  inline virtual bool blocking() { return m_blocking; }
  inline virtual void blocking(bool b) { m_blocking = b; }
  
 protected:
  bool m_blocking;
  
 private:
  Tile( Tile const& tile ) = delete;
  Tile& operator=( Tile const& tile ) = delete;
};

#endif
