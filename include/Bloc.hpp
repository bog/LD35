/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef BLOC_HPP
#define BLOC_HPP
#include <iostream>
#include <Shape.hpp>

/**
 *  Bloc that can be eat by the player.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Bloc : public Shape
{
 public:
  /**
   * @param core Game core.
   * @param r row where the shape will be.
   * @param c column where the shape will be.
   */
  explicit Bloc(Core* core, size_t r, size_t c);
  virtual ~Bloc();

  virtual void update() override;
  virtual void draw(Render* render) override;
 protected:
  
 private:
  Bloc( Bloc const& bloc ) = delete;
  Bloc& operator=( Bloc const& bloc ) = delete;
};

#endif
