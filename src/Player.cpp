#include <Player.hpp>
#include <Bloc.hpp>
#include <LevelScene.hpp>

Player::Player(LevelScene* level, Core* core, float r, float c)
  : Shape(core
	  , c * BLOC_WIDTH
	  , r * BLOC_HEIGHT
	  , BLOC_WIDTH
	  , BLOC_HEIGHT)
  , m_level(level)
  , m_walk_delay(125)
  , m_attach_delay(125)
{
  m_id = EntityType::Player;
  m_zorder = PLAYER_ZORDER;
  m_shape.setFillColor(sf::Color::Green);
}

bool Player::canWalk(int r, int c)
{
  for(Bloc* bloc : m_blocs)
    {
      bool blocked = false;
      for( Entity* e : m_level->entity(bloc->coords().x+r, bloc->coords().y+c) )
	{
	  if( e->id() == EntityType::Bloc )
	    {
	      auto itr = std::find(m_blocs.begin(), m_blocs.end(), e );
	      if( itr == m_blocs.end() )
		{
		  blocked = true;
		  break;
		}
	    }
	}

      if( blocked
	  || !m_level->isAccessible(bloc->coords().x + r, bloc->coords().y + c) )
	{

	  return false;
	}
    }
  
  std::vector<Entity*> entities = m_level->entity(coords().x + r, coords().y + c);
  
  for(Entity* entity : entities)
    {
      bool in_player_bloc = false;
      
      for(Bloc* bloc : m_blocs)
      	{
	  // sf::Vector2f c_shape = static_cast<Shape*>(entity)->coords();
	  
	  if(entity == bloc){ in_player_bloc = true; }

	  // if(entity->id() == EntityType::Bloc
	  //    && bloc->coords().x + r == c_shape.x
	  //    && bloc->coords().y + c == c_shape.y)
	  //   {
	  //     return false;
	  //   }
      	}
      
      if(entity->id() == EntityType::Bloc
	 && !in_player_bloc)
	{
	  return false;
	}
    }
  
  return m_level->isAccessible(coords().x + r, coords().y + c);
}

void Player::walk(float r, float c)
{
  if(m_walk_clock.getElapsedTime().asMilliseconds() >= m_walk_delay)
    {
      m_rect.top += r * BLOC_HEIGHT;
      m_rect.left += c * BLOC_WIDTH;
      
      for(Bloc* bloc : m_blocs)
      	{
      	  bloc->move(c * BLOC_WIDTH, r * BLOC_HEIGHT);
      	}
      
      m_walk_clock.restart();
    }
}


void Player::attach(Bloc* bloc)
{
  if(bloc == nullptr)
    {
      throw std::invalid_argument("Can't attach a bloc (invalid bloc).");
    }

  if(m_attach_clock.getElapsedTime().asMilliseconds() >= m_attach_delay)
    {
      m_blocs.push_back(bloc);
      m_attach_clock.restart();
    }
}

bool Player::canAttach(Bloc* bloc)
{
  auto itr = std::find(m_blocs.begin(), m_blocs.end(), bloc);

  // already attached
  if( itr != m_blocs.end() )
    {
      return false;
    }

  // at the same position as the player
  if(bloc->coords().x == coords().x
     && bloc->coords().y == coords(). y)
    {
      return false;
    }
  

  Bloc* bloc_nearest = nullptr;
  int smallest_dist = abs(bloc->coords().x - coords().x)
    + abs(bloc->coords().y - coords().y);

  for(Bloc* b : m_blocs)
    {
      int dist = abs(bloc->coords().x - b->coords().x)
	+ abs(bloc->coords().y - b->coords().y);
      
      if(dist < smallest_dist)
	{
	  bloc_nearest = b;
	  smallest_dist = dist;
	}
    }

  // too far
  if( smallest_dist > 1 )
    {
      return false;
    }

  // on the bloc the nearest
  if(bloc_nearest != nullptr
     && bloc_nearest->coords().x == bloc->coords().x
     && bloc_nearest->coords().y == bloc->coords().y)
    {
    }
  
  return true;
}

/*virtual*/ void Player::update()
{
  
}

/*virtual*/ void Player::draw(Render* render)
{
  Shape::draw(render);
}


Player::~Player()
{
  
}
