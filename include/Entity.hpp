/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#ifndef ENTITY_HPP
#define ENTITY_HPP
#include <iostream>
#include <SFML/Graphics/RenderWindow.hpp>
#include <Core.hpp>

/**
 * Kind of entity that could be found in the game.
 */
enum class EntityType
  {
    Unknown
      , Ground
      , Wall
      , Finish
      , Bloc
      , Player
  };

/**
 * Order of the different entities' zorder.
 */
enum
  {
    BASIC_ZORDER = 0
    , BLOC_ZORDER
    , PLAYER_ZORDER
  };

/**
 * Game object able to be drawn and to be updated.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Entity
{
 public:
  explicit Entity(Core* core);
  virtual ~Entity();

  /**
   * Update the entity.
   */
  virtual void update() = 0;

  /**
   * Draw the entity on the screen.
   *
   * @param render the Render used to draw the entity.
   */
  virtual void draw(Render* render) = 0;

  inline virtual int zorder() const { return m_zorder; }
  inline virtual void zorder(int z) { m_zorder = z; }
  
  inline EntityType id() const { return m_id; }
  inline void id(EntityType id) { m_id = id; }
  
 protected:
  Core* m_core;
  int m_zorder;
  EntityType m_id;
  
 private:
  Entity( Entity const& entity ) = delete;
  Entity& operator=( Entity const& entity ) = delete;
};

#endif
