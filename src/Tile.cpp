/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Tile.hpp>
#include <stdexcept>

Tile::Tile(Core* core, size_t r, size_t c)
  : Shape(core
	  , c * BLOC_WIDTH
	  , r * BLOC_HEIGHT
	  , BLOC_WIDTH
	  , BLOC_HEIGHT)
  , m_blocking(false)
{
  if(r < 0 || r > N_BLOC_HEIGHT
     || c < 0 || r > N_BLOC_WIDTH)
    {
      throw std::domain_error("("
			       + std::to_string(r)
			       + ", "
			       + std::to_string(c)
			       + ") is out of the screen.");
    }

  m_shape.setSize(sf::Vector2f(m_rect.width - 1, m_rect.height - 1));
  m_shape.setOutlineColor(sf::Color::Black);
  m_shape.setOutlineThickness(1.0);

}

Tile::~Tile()
{
  
}
