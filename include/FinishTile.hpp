/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef FINISHTILE_HPP
#define FINISHTILE_HPP
#include <iostream>
#include <Tile.hpp>

/**
 * Tile representing the end of the current level.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class FinishTile : public Tile
{
 public:
  explicit FinishTile(Core* core, size_t r, size_t c);
  virtual ~FinishTile();

  virtual void update();
  virtual void draw(Render* render);

 protected:
  
 private:
  FinishTile( FinishTile const& finishtile ) = delete;
  FinishTile& operator=( FinishTile const& finishtile ) = delete;
};

#endif
