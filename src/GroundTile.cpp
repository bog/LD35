/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <GroundTile.hpp>

GroundTile::GroundTile(Core* core, float r, float c)
  : Tile(core, r, c)
{
  m_id = EntityType::Ground;
  m_shape.setFillColor( sf::Color(100, 90, 90) );
}

/**virtual*/ void GroundTile::update()
{

}

/**virtual*/ void GroundTile::draw(Render* render)
{
  Tile::draw(render);
}


GroundTile::~GroundTile()
{
  
}
