#include <Shape.hpp>

/*explicit*/ Shape::Shape(Core* core, float x, float y, float w, float h)
  : Entity(core)
  , m_rect( sf::Rect<float>(x, y, w, h) )
  , m_shape( sf::Vector2f(m_rect.width, m_rect.height) )
{
}

/*explicit*/ Shape::Shape(Core* core, float w, float h)
  : Shape(core, 0.f, 0.f, w, h)
{
}

/*explicit*/ Shape::Shape(Core* core)
  : Shape(core, 0.f, 0.f)
{
}

sf::Vector2f Shape::coords() const
{
  return coords(0, 0);
}

sf::Vector2f Shape::coords(int r_offset, int c_offset) const
{
  sf::Vector2f coords(  static_cast<int>(m_rect.top/BLOC_HEIGHT + r_offset)
			, static_cast<int>(m_rect.left/BLOC_WIDTH + c_offset) );
  
  return coords;
}


/*virtual*/ void Shape::draw(Render* render)
{
  m_shape.setPosition( sf::Vector2f(m_rect.left, m_rect.top) );
  render->draw(*this);
}

Shape::~Shape()
{
  
}
