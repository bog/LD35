/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#ifndef GROUNDTILE_HPP
#define GROUNDTILE_HPP
#include <iostream>
#include <Tile.hpp>

/**
 * Tile that is used as a basic ground.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class GroundTile : public Tile
{
 public:
  
  /**
   * @param r row where the tile while be.
   * @param c column where the tile while be.
   */
  explicit GroundTile(Core* core, float r, float c);
  virtual ~GroundTile();

  virtual void update();
  virtual void draw(Render* render);
  
 protected:
  
 private:
  GroundTile( GroundTile const& groundtile ) = delete;
  GroundTile& operator=( GroundTile const& groundtile ) = delete;
};

#endif
