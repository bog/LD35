/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Render.hpp>
#include <Shape.hpp>

Render::Render(sf::RenderWindow* window)
  : m_window(window)
{
  
}

void Render::draw(sf::Drawable const& drawable)
{
  m_window->draw(drawable);
}

void Render::draw(Shape const& shape)
{
  m_shapes.push_back(&shape);
}

void Render::display()
{
  std::sort(m_shapes.begin(), m_shapes.end(), [](Shape const* s1, Shape const* s2){
      return s1->zorder() < s2->zorder();
    });
  
  for(Shape const* shape : m_shapes)
    {
      draw( shape->shape() );
    }
  
  m_window->display();

  m_shapes.clear();
}

void Render::clear(sf::Color const& color)
{
  m_window->clear(color);
}

Render::~Render()
{
  
}
