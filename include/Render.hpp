/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RENDER_HPP
#define RENDER_HPP
#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

class Shape;

/**
 * Render sf::Drawable in the screen.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Render
{
 public:
  explicit Render(sf::RenderWindow* window);
  virtual ~Render();

  void draw(sf::Drawable const& drawable);
  
  void draw(Shape const& shape);
  
  void display();
  
  void clear(sf::Color const& color);
  
 protected:
  sf::RenderWindow* m_window;
  std::vector<Shape const*> m_shapes;
    
 private:
  Render( Render const& render ) = delete;
  Render& operator=( Render const& render ) = delete;
};

#endif
