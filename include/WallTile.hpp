/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#ifndef WALLTILE_HPP
#define WALLTILE_HPP
#include <iostream>
#include <Tile.hpp>

/**
 * Tile that is used as a basic wall.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class WallTile : public Tile
{
 public:
  
  /**
   * @param core the game core.
   * @param r row where the tile will be.
   * @param c column where the tile will be.
   */
  explicit WallTile(Core* core, size_t r, size_t c);
  virtual ~WallTile();

  virtual void update();
  virtual void draw(Render* render);
  
 protected:
  
 private:
  WallTile( WallTile const& walltile ) = delete;
  WallTile& operator=( WallTile const& walltile ) = delete;
};

#endif
