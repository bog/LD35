/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>

#include <Controller.hpp>
#include <Render.hpp>

class Scene;

constexpr unsigned int WINDOW_WIDTH = 640;
constexpr unsigned int WINDOW_HEIGHT = 480;
constexpr unsigned int BLOC_WIDTH = 32;
constexpr unsigned int BLOC_HEIGHT = 32;
constexpr unsigned int N_BLOC_WIDTH = WINDOW_WIDTH/BLOC_WIDTH;
constexpr unsigned int N_BLOC_HEIGHT = WINDOW_HEIGHT/BLOC_HEIGHT;

/**
 * Main class of the game.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Core
{
 public:
  explicit Core();
  virtual ~Core();

  /**
   * Start the game.
   */
  void start();

  /**
   * Stop the game.
   */
  void stop();
  
  inline Controller* controller() { return m_controller.get(); }
  inline Render* render() { return m_render.get(); }
  inline void setTitle(std::string title) { m_window->setTitle(title); }
  
 protected:
  std::unique_ptr<sf::RenderWindow> m_window;
  std::unique_ptr<Controller> m_controller;
  std::unique_ptr<Render> m_render;
  bool m_is_running;
  std::unique_ptr<Scene> m_scene;

  /**
   * Game loop of the game.
   * Update an draw the game while it's running.
   */
  void loop();

  /**
   * Draw all the sprite of the game.
   */
  void draw();

  /**
   * Update the game.
   */
  void update();
  
 private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
