/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LevelScene.hpp>
#include <Config.hpp>
#include <Player.hpp>
#include <Bloc.hpp>
#include <GroundTile.hpp>
#include <WallTile.hpp>
#include <FinishTile.hpp>

LevelScene::LevelScene(Core* core)
  : Scene(core)
  , m_player(nullptr)
  , m_level(1)
{
  loadLevel(m_level);

  m_core->controller()->link({{sf::Keyboard::R}, KeyState::Pressed, [this](){
	loadLevel(m_level);
      }});
  
  m_core->controller()->link({{sf::Keyboard::Up}, KeyState::Pressed, [this](){
	sf::Vector2f c(-1, 0);
	
	if( m_player->canWalk(c.x, c.y) )
	  {
	    m_player->walk(c.x, c.y);
	  }
      }});
  
  m_core->controller()->link({{sf::Keyboard::Down}, KeyState::Pressed, [this](){
	sf::Vector2f c(1, 0);
	
	if( m_player->canWalk(c.x, c.y) )
	  {
	    m_player->walk(c.x, c.y);
	  }
      }});
  
  m_core->controller()->link({{sf::Keyboard::Left}, KeyState::Pressed, [this](){
	sf::Vector2f c(0, -1);
	
	if( m_player->canWalk(c.x, c.y) )
	  {
	    m_player->walk(c.x, c.y);
	  }

      }});
  
  m_core->controller()->link({{sf::Keyboard::Right}, KeyState::Pressed, [this](){
	sf::Vector2f c(0, 1);
	
	if( m_player->canWalk(c.x, c.y) )
	  {
	    m_player->walk(c.x, c.y);
	  }
	
      }});
  
  m_core->controller()->link({{sf::Keyboard::Space}, KeyState::Pressed, [this](){

	for(auto& entity : m_entities)
	  {
	    if(entity->id() == EntityType::Bloc)
	      {
		Bloc* bloc = static_cast<Bloc*>( entity.get() );

		if( m_player->canAttach(bloc) )
		  {
		    m_player->attach(bloc);
		  }
	      }
	  }
      }
    });
}

bool LevelScene::isValid(size_t r, size_t c) const
{
  return ! (r < 0 || r >= N_BLOC_HEIGHT
	    || c < 0 || c >= N_BLOC_WIDTH);
}

bool LevelScene::isAccessible(size_t r, size_t c) const
{
  if( !isValid(r, c) )
    {
      throw std::domain_error("Tile ("
			      + std::to_string(r)
			      + ", "
			      + std::to_string(c)
			      + ") don't exists (out of the map)");
    }
  
  return ! m_tiles[r][c]->blocking();
}

void LevelScene::loadLevel(int lvl)
{
  m_entities.clear();
  m_player = nullptr;
  m_core->setTitle( "Level " + std::to_string(lvl) );
  initializeLevel("lvl" + std::to_string(lvl));
}

void LevelScene::initializeLevel(std::string level_name)
{
  std::string path = std::string(ASSETS_DIR) + "/levels/" + level_name + ".png";
  
  sf::Image image;
  image.loadFromFile(path);
  
  sf::Color color;
  
  for(size_t i=0; i<N_BLOC_HEIGHT; i++)
    {
      for(size_t j=0; j<N_BLOC_WIDTH; j++)
  	{
	  color = image.getPixel(j, i);
	  EntityType type = getIDFromColor(color);
	  
	  m_entities.push_back( std::move( getTileFromID(i, j, type) ) );
	  m_tiles[i][j] = static_cast<Tile*>( m_entities.back().get() );
	  
	  switch(type)
	    {
	    case EntityType::Player:
	      m_entities.push_back( std::make_unique<Player>(this, m_core, i, j) );
	      m_player = static_cast<Player*>( m_entities.back().get() );
	      break;
	    case EntityType::Bloc:
	      m_entities.push_back( std::make_unique<Bloc>(m_core, i, j) );
	      break;
	      
	    default: // tiles
	      break;
	    }
	}
    }
}

EntityType LevelScene::getIDFromColor(sf::Color color)
{
  if(color == sf::Color::White)
    {
      return EntityType::Ground;
    }
    
  if(color == sf::Color::Black)
    {
      return EntityType::Wall;
    }

  if(color == sf::Color::Green)
    {
      return EntityType::Player;
    }

  if(color == sf::Color::Blue)
    {
      return EntityType::Bloc;
    }
  
  if(color == sf::Color::Yellow)
    {
      return EntityType::Finish;
    }

  return EntityType::Unknown;

}

std::unique_ptr<Tile> LevelScene::getTileFromID(size_t r, size_t c, EntityType type)
{
  std::unique_ptr<Tile> tile;
  
  switch(type)
    {
    case EntityType::Wall:
      tile = std::make_unique<WallTile>(m_core, r, c);
      break;
    case EntityType::Finish:
      tile = std::make_unique<FinishTile>(m_core, r, c);
      break;
    case EntityType::Bloc:
    case EntityType::Player:
    case EntityType::Ground:
      tile = std::make_unique<GroundTile>(m_core, r, c);
      break;
    default:
      throw std::invalid_argument("Bad type given to getTileFromID.");
      break;
    }

  return tile;
}

std::vector<Entity*> LevelScene::entity(size_t r, size_t c) const
{
  std::vector<Entity*> vec;
  
  if( ! isValid(r, c) )
    {
      throw std::domain_error("Bad coordinates ("
			      + std::to_string(c)
			      + ", "
			      + std::to_string(c)
			      +")");
    }
  
  for(auto& entity : m_entities)
    {
      sf::Vector2f coord = static_cast<Shape*>( entity.get() )->coords();
      
      if(coord.x ==r && coord.y == c)
	{
	  vec.push_back( entity.get() );
	}
    }

  return vec;
}

/*virtual*/ void LevelScene::update()
{
  Scene::update();

  for(Entity* e : entity(m_player->coords().x, m_player->coords().y) )
    {
      if(e->id() == EntityType::Finish)
	{
	  m_level++;
	  if(m_level > LEVEL_MAX){ m_level = 1; }
	  loadLevel(m_level);
	  break;
	}
    }
}

/*virtual*/ void LevelScene::draw(Render* render)
{
  render->clear(sf::Color::White);
  Scene::draw(render);
}

LevelScene::~LevelScene()
{
  
}
