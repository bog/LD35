/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#include <Core.hpp>
#include <LevelScene.hpp>

/*explicit*/ Core::Core()
  : m_window( std::make_unique<sf::RenderWindow>(sf::VideoMode(WINDOW_WIDTH
							      , WINDOW_HEIGHT)
						, "LD"
						, sf::Style::Titlebar) )
  , m_controller( std::make_unique<Controller>( m_window.get() ) )
  , m_render( std::make_unique<Render>( m_window.get() ) )
  , m_is_running(false)
  , m_scene( std::make_unique<LevelScene>(this) )

    
{
  action_t stop_action = [this](){
    stop();
  };
  
  m_controller->link({{sf::Keyboard::Escape}, KeyState::Pressed, stop_action});
  m_controller->link({{sf::Keyboard::LControl, sf::Keyboard::C}
      , KeyState::Pressed, stop_action});
  m_controller->link({{sf::Keyboard::LControl, sf::Keyboard::Q}
      , KeyState::Pressed, stop_action});
  m_controller->closeAction(stop_action);
}

void Core::start()
{
  m_is_running = true;
  loop();
}

void Core::stop()
{
  m_is_running = false;
}

void Core::update()
{
  if(m_scene != nullptr){ m_scene->update(); }
  m_controller->update();
}

void Core::draw()
{
  if(m_scene != nullptr){ m_scene->draw(m_render.get()); }
}

void Core::loop()
{
  while(m_is_running)
    {
      update();     
      draw();
      m_render->display();
    }
}

Core::~Core()
{
  
}
