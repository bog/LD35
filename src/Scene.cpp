#include <Scene.hpp>

Scene::Scene(Core* core)
  : m_core(core)
{
  
}

/*virtual*/ void Scene::update()
{
  for(auto& entity : m_entities)
    {
      entity->update();
    }
}

/*virtual*/ void Scene::draw(Render* render)
{
    for(auto& entity : m_entities)
    {
      entity->draw(render);
    }
}

Scene::~Scene()
{
  
}
