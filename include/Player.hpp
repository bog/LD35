/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <vector>
#include <Shape.hpp>
#include <SFML/Graphics.hpp>

class Bloc;
class LevelScene;

/**
 * Character controlled by the player.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Player : public Shape
{
 public:
  /**
   * Construct the player using a starting position.
   * @param r Row where the player will start.
   * @param c Column where the player will start.
   */
  explicit Player(LevelScene *level, Core* core, float r, float c);
  virtual ~Player();

  virtual void update() override;
  virtual void draw(Render* render) override;

  /**
   * Make the player walk in the level grid.
   * @param r The row to add to the player row.
   * @param c The column to add to the player column.
   */
  void walk(float r, float c);

  /**
   * Test if the player can walk in a tile.
   * @param r Row movement.
   * @param c Column movement.
   * @return True if the movement is possible, false otherwise.
   */
  bool canWalk(int r, int c);
  
  /**
   * Attach a bloc to the player.
   * @param bloc Bloc to attach to the player.
   */
  void attach(Bloc* bloc);

  /**
   * Test if the player can attach himself to a bloc.
   * @param bloc Bloc the player want to be attached to.
   * @return True if the attachment is possible, false otherwise.
   */
  bool canAttach(Bloc* bloc);

 protected:
  LevelScene* m_level;
  sf::Clock m_walk_clock;
  sf::Clock m_attach_clock;
  int const m_walk_delay;
  int const m_attach_delay;

  std::vector<Bloc*> m_blocs;
  
 private:
  Player( Player const& player ) = delete;
  Player& operator=( Player const& player ) = delete;
};

#endif
