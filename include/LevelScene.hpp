/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#ifndef LEVELSCENE_HPP
#define LEVELSCENE_HPP
#include <iostream>
#include <Scene.hpp>

class Tile;
class Player;

constexpr int LEVEL_MAX = 8;
/**
 * Scene representing a level of the game.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class LevelScene : public Scene
{
 public:
  explicit LevelScene(Core* core);
  virtual ~LevelScene();
  
  virtual void update() override;
  virtual void draw(Render* render) override;

    /**
   * Test if a coordinate is vaild.
   * A invalid coordinate is a coordinate that
   * is out of the level map.
   *
   * @param r The row of the location to test.
   * @param c The column of the location to test.
   * @return True if the coordinate is valid, false otherwise.
   */
  bool isValid(size_t r, size_t c) const;
  
  /**
   * Indicate if a tile is accessible.
   * Tiles that are obstacle are not accessible.
   *
   * @param r Row of where is the Tile to check.
   * @param c Column of where is the Tile to check.
   * @return True if the Tile[i][j] is accessible, false otherwise.
   */
  bool isAccessible(size_t r, size_t c) const;

  inline Tile* tile(size_t r, size_t c) { return m_tiles[r][c]; }

  /**
   * Access to the entity in a tile.
   *
   * @param r The row to check.
   * @param c The column to check.
   */
  std::vector<Entity*> entity(size_t r, size_t c) const;
  
 protected:
  Tile* m_tiles[N_BLOC_HEIGHT][N_BLOC_WIDTH];
  Player* m_player;
  int m_level;
  
  /**
   * Initialize the level from a png file.
   * The level is read directly from the pixels of 
   * the picture.
   *
   * @note Levels are in the assets/levels directory. Their name
   * correspond to their filename. For instance, the level "my_level"
   * correspond to the file "assets/evels/my_level.png"
   *
   * @param level_name the name of the level.
   */
  void initializeLevel(std::string level_name);

  /**
   * Load a level of the game.
   *
   * @param lvl The level to load.
   * @note The level must be called lvl[lvl].png.
   */
  void loadLevel(int lvl);
  
  /**
   * Convert a color into the corresponding entity type.
   * For instance a black pixel correspond to a wall.
   *
   * @param color the color to convert.
   * @return an entity type that correspond to color.
   */
  EntityType getIDFromColor(sf::Color color);
  
  /**
   * Create a Tile corresponding to a given type.
   * @param r row of the Tile to create.
   * @param c columns of the Tile to create.
   * @param type the type of the Tile to create.
   * @return a Tile depending on the color.
   */
  std::unique_ptr<Tile> getTileFromID(size_t r, size_t c, EntityType type);
    
 private:
  LevelScene( LevelScene const& levelscene ) = delete;
  LevelScene& operator=( LevelScene const& levelscene ) = delete;
};

#endif
