/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <iostream>
#include <Entity.hpp>

/**
 * Entity with a shape and a hitbox.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Shape : public Entity
{
 public:
  explicit Shape(Core* core, float x, float y, float w, float h);
  explicit Shape(Core* core, float w, float h);
  explicit Shape(Core* core);
  
  virtual ~Shape();

  virtual void update() override = 0;
  virtual void draw(Render* render) override;

  inline virtual sf::RectangleShape const& shape() const { return m_shape; }
  inline virtual void shape(sf::RectangleShape shape) { m_shape = shape; }

  inline virtual sf::Rect<float> const& rect() const { return m_rect; }
  inline virtual void rect(sf::Rect<float> rect) { m_rect = rect; }

  /**
   * Move the shape.
   * @param x the X-Axis movement.
   * @param y the Y-Axis movement.
   */
  inline virtual void move(float x, float y) { m_rect.left += x; m_rect.top += y; }
  
  /**
   * Get the shape coordinates (row and column)
   * and it to it an offset.
   *
   * @param r_offset Row offset to add to the coordinates.
   * @param c_offset Column offset to add to the coordinates.
   * @return The shape coordinates.
   */
  sf::Vector2f coords(int r_offset, int c_offset) const;

  /**
   * Coordinate without offset.
   * @see sf::Vector2f coords(int r_offset, int c_offset) const;
   */
  sf::Vector2f coords() const;

 protected:
  sf::Rect<float> m_rect;
  sf::RectangleShape m_shape;
  
 private:
  Shape( Shape const& shape ) = delete;
  Shape& operator=( Shape const& shape ) = delete;
};

#endif
