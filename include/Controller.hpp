/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP
#include <iostream>
#include <functional>
#include <unordered_map>
#include <vector>

#include <SFML/Graphics.hpp>

typedef std::function<void(void)> action_t;

/**
 * State of a key.
 */
enum class KeyState
  {
    Pressed
      , Released
  };

/**
 * Request to link an event
 * to an action.
 */
struct Request
{
  std::vector<int> keys; /**< Keys involved.  */
  KeyState state; /**< State the keys must have. */
  action_t action; /**< Action when all the keys are in the right state. */
};
  
/**
 * Read inputs and link them to actions.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Controller
{
 public:
  explicit Controller(sf::RenderWindow* window);
  virtual ~Controller();

  /**
   * Listen inputs.
   */
  void update();

  /**
   * Link an action to a combinaison of keys.
   * @param request corresponding.
   */
  void link(Request request);

  inline sf::Event& event() { return m_event; }
  inline action_t closeAction(){ return m_close_action; }
  inline void closeAction(action_t action){ m_close_action = action; }
  
 protected:
  KeyState m_keys[sf::Keyboard::KeyCount];
  std::vector<Request> m_requests;
    
  sf::RenderWindow* m_window;
  sf::Event m_event;

  action_t m_close_action;
  
 private:
  Controller( Controller const& controller ) = delete;
  Controller& operator=( Controller const& controller ) = delete;
};

#endif
