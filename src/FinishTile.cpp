/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <FinishTile.hpp>

FinishTile::FinishTile(Core* core, size_t r, size_t c)
  : Tile(core, r, c)
{
  m_id = EntityType::Finish;
  m_shape.setFillColor(sf::Color::Yellow);
}

/*virtual*/ void FinishTile::update()
{
}

/*virtual*/ void FinishTile::draw(Render* render)
{
  Tile::draw(render);
}



FinishTile::~FinishTile()
{
  
}
