/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#include <Controller.hpp>

Controller::Controller(sf::RenderWindow* window)
  : m_window(window)
{
  for(size_t i=0; i<sf::Keyboard::KeyCount; i++)
    {
      m_keys[i] = KeyState::Released;
    }
}

void Controller::link(Request request)
{
  m_requests.push_back(request);
}
    
void Controller::update()
{
  while( m_window->pollEvent(m_event) )
    {
      switch(m_event.type)
	{
	case sf::Event::Closed:
	  m_close_action();
	  break;

	case sf::Event::KeyPressed:
	  m_keys[m_event.key.code] = KeyState::Pressed;
	  break;

	case sf::Event::KeyReleased:
	  m_keys[m_event.key.code] = KeyState::Released;
	  break;

	default:
	  break;
	}
    }

  for(Request& request : m_requests)
    {
      bool condition_fulfilled = true;
      
      for(int keycode : request.keys)
	{
	  if(m_keys[keycode] != request.state)
	    {
	      condition_fulfilled = false;
	      break;
	    }
	}

      if(condition_fulfilled)
	{
	  request.action();
	}
    }
}

Controller::~Controller()
{
  
}
